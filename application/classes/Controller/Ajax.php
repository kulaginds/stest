<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller {

	// действие по умолчанию для данного контроллера
	public function action_index()
	{
		if (Request::initial()->is_ajax()) // выполняем только если запрос был через Ajax
        {
            $this->response->body('index action');
        }
	}

	// действие captcha
	// проверяет значение капчи, которое ввел пользователь
	public function action_captcha()
	{
		// инициализация сессии
		$session = Session::instance();

		// получаем поле value
		$value = Arr::get($_POST, 'value', '');
		// проверяем, чтобы оно не было пустым
		if (empty($value)) {
			$this->response->body('Значение капчи не пришло.');
			return;
		}

		// получаем истинное значение капчи
		$keystring = $session->get('captcha_keystring', '');

		// сверяем истинное значение капчи с тем, что прислал пользователь
		if ((strlen($keystring) > 0) && ($keystring == $value)) {
			$this->response->body(1);
		} else {
			$this->response->body('Введенные символы не совпадают с символами на картинке.');
		}
	}

	// действие send
	// отправляет сообщение с помощью cURL
	public function action_send()
	{
		// инициализация сессии
		$session = Session::instance();

		// получаем поле data
		$data = Arr::get($_POST, 'data', '');
		// проверям, чтобы оно не было пустым
		if (empty($data)) {
			$this->response->body('Данные не пришли.');
			return;
		}

		// разбиваем строку в формате {name}::{message}::{email}::{captcha} на элементы массива
		list($name, $message, $email, $captcha) = explode("::", $data);

		if (is_null($name) || is_null($message) || is_null($email) || is_null($captcha)) {
			$this->response->body("Пришли не все параметры.");
			return;
		}
		
		$params = array(
			"name" => $name,
			"message" => $message,
			"email" => $email,
			"captcha" => $captcha,
		);	
	
		$validatioin = Validation::factory($params);
		$validatioin->rule(TRUE, 'not_empty')
		       ->rule('email', 'email');

		// проверяем, чтобы все переменные имели значение
		if (!$validatioin->check()) {
			$errors = $validatioin->errors();
			$errs = "";
			foreach ($errors as $key => $value) {
				$errs .= "Поле ".$key." содержит ошибку: ".$value."\n";
			}
			$this->body->response($errs);
			return;
		}

		// инициализация сессии и получение поля captcha_keystring - истинного значения капчи
		$keystring = Session::instance()->get('captcha_keystring', '');

		// проверяем капчу: то, что прислал пользователь с тем, что было на самом деле
		if ((strlen($keystring) == 0) || ($keystring != $captcha)) {
			$this->response->body('Введенные символы не совпадают с символами на картинке.');
			return;
		}

		// формируем поле data для скрипта cURL
		unset($params['captcha']);

		$url = URL::base("http", FALSE).URL::site("curl");

		//$url = "http://".$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"]."/curl";

		// инициализация cURL
		$ch = curl_init($url);

		// установка параметров
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвращать ответ
		curl_setopt($ch, CURLOPT_POST, 1); // тип запроса: POST

		 // данные с полем data, с закодированным в формате JSON массивом $params
		$params = json_encode($params);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "data=".$params);

		// ошибка по умолчанию
		$error = 'Сервер вернул пустой ответ.';

		// проверям, удалось ли сделать запрос на скрипт
		$result = curl_exec($ch);
		if ($result) {
			// декодируем результат
			$result = json_decode($result, TRUE);
		} else {
			$this->response->body($error);
			return;
		}

		curl_close($ch);

		// проверяем, что ответил скрипт
		if (!is_null($result) && array_key_exists("response", $result)) { // успешное сообщение
			$this->response->body($result["response"]);
		} else {
			if (!is_null($result) && array_key_exists("error", $result)) { // сообщение об ошибке
				$error = $result["error"];
			}
			$this->response->body($error);
		}
	}

}
