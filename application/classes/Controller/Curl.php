<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Curl extends Controller {

	// действие по умолчанию для данного контроллера
	public function action_index()
	{
		// получаем и декодируем поле data
		$data = Arr::get($_POST, 'data', '');
		$data = json_decode($data, TRUE);
		
		if (is_null($data)) {
			$this->setError("Не пришли данные.");
			return;
		}
		
		// проверяем, все ли поля есть у сообщения
		$keys = array("name", "message", "email");
		for ($i=0; $i<count($keys); $i++) {
			if (!array_key_exists($keys[$i], $data)) {
				$this->setError("Нет параметра: ".$keys[$i]);
				return;
			}
		}
		
		// создаём модель сообщения
		$msg = new Model_Messages();
		// заполняем поля
		$msg->name = $data["name"];
		$msg->message = $data["message"];
		$msg->email = $data["email"];
		$msg->date_created = date("Y-m-d G:i:s");

		// сохраняем
		if ($msg->save()) {
			$this->setResponse("Ваше сообщение отправлено успешно!");
		} else {
			$this->setError("Не удалось сохранить сообщение.");
		}
	}

	// универсальный метод для данного контроллера
	// формирует массив с полем error, кодирует в формат JSON и возвращает пользователю
	protected function setError($msg) {
		$result = json_encode(array("error" => $msg));
		$this->response->body($result);
	}

	// универсальный метод для данного контроллера
	// формирует массив с полем response, кодирует в формат JSON и возвращает пользователю
	protected function setResponse($msg) {
		$result = json_encode(array("response" => $msg));
		$this->response->body($result);
	}

}