<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_Template {

	public $template = "main";

	public function action_index()
	{
		// инициализируем сессию
		$session = Session::instance();

		// добавляем в шаблон переменные
		$this->template->session_name = $session->name();
		$this->template->session_id = $session->id();
	}

}