<?php defined('SYSPATH') or die('No direct script access.');

class Task_Cron extends Minion_Task {

	protected function _execute(array $params)
    {
        // SQL-запрос, который реализует модель Messages
        //SELECT * FROM messages WHERE sent=0 AND date_created >= (NOW() - INTERVAL 10 MINUTE)
        // создаем модель
        $messages = new Model_Messages();
        // устанавливаем условия поиска
        $messages->where("sent", "=", 0) // чтобы sent=0
                //  и чтобы date_created была больше либо равна значению ТЕКУЩЕЕ ВРЕМЯ - 10 МИНУТ
                 ->and_where("date_created", ">=", DB::expr("NOW() - INTERVAL 10 MINUTE"));
        $result = $messages->find_all(); // извлекаем все строки результата

        // массив провайдеров эл. почты, которые не позволены
        $patterns = array("mail.ru", "rambler.ru", "yandex.ru");
        // тема сообщения для тех, у кого почта от одного из вышеперечисленных провайдеров
        $subject1 = "К сожалению, мы не можем вам помочь";
        // тема сообщения, если не из тех, кто в массиве выше
        $subject2 = "Всё отлично";

        // счетсчики, сколько удачно отправлено сообщений, а сколько нет
        $success = 0;
        $error = 0;

        // выбираем каждое сообщение
        foreach($result as $message) {
            // делим эл. адрес на две части: имя пользователя и провайдера
            $email = explode("@", $message->get("email"));
            $provider = $email[1]; // выбираем провайдера

            // проверяем, является ли провайдер не позволительным для нас
            $is_pattern = in_array($provider, $patterns);
            // массив параметров для шаблона
            $params = array(
                "name" => $message->get("name"),
                "text" => $message->get("message"),
                "provider" => $provider,
            );
            $email = $message->get("email");
            // если провайдер не позволителен для нас, то ставим тему $subject1, иначе $subject2
            $subject = $is_pattern?$subject1:$subject2;
            // формируем текст сообщения, загрузив шаблон
            $msg = View::factory($is_pattern?"mail1":"mail2", $params)->render();

            // отправляем сообщение
            if (mail($email, $subject, $msg)) {
                // если сообщение отправлено
                $success++;
                $message->sent = 1;
                $message->save();
            } else {
                // если не отправлено
                $error++;
            }
        }

        // печатаем счетсчики в консоль
        Minion_CLI::write($success.":".$error);
    }

}