<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Отправлятель писем</title>
	<style type="text/css">
	body {
		font: 14px Arial, Verdana, sans-serif;
		color: #222;
		margin: 0;
		padding: 0;
	}

	input[type="text"], textarea {
		border: 1px solid #999;
	}

	#main {
		width: 600px;
		margin: 40px auto;
	}

	#main div.body {
		padding: 20px 0;
		margin: 20px 0;
		border-top: 1px solid #999;
		border-bottom: 1px solid #999;
	}
	input.error, textarea.error {
		border-color: red;
	}
	p.error {
		color: red;
		margin-top: 0;
	}
	#modal {
		display: none;
		position: absolute;
		top: 0;
		width: 100%;
		height: 100%;
	}
	#modal div.bg {
		position: absolute;
		width: 100%;
		height: 100%;
		background: rgba(0, 0, 0, 0.8);
	}
	#modal div.body {
		position: relative;
		padding: 20px;
		top: 50%;
	    left: 50%;
	    margin-top: -160px;
	    margin-left: -175px;
	    width: 350px;
	    height: 320px;
		background: #fff;
		border: 1px solid #999;
	}
	</style>
</head>
<body>
	<div id="main">
		<div class="head">
			<h1>Отправлятель писем</h1>
			<strong>Отправляет письма всем, кроме mail.ru, rambler.ru или yandex.ru</strong>
		</div>
		<noscript>Для работы формы необходима поддержка JavaScript.</noscript>
		<div class="body">
			<form id="form">

				<label for="name">Ф.И.О.:</label><br>
				<input type="text" id="name"><br>
				<p class="error" id="name-error"></p>

				<label for="message">Сообщение:</label><br>
				<textarea cols="40" rows="10" id="message"></textarea><br>
				<p class="error" id="message-error"></p>

				<label for="email">Эл. почта:</label><br>
				<input type="text" id="email"><br>
				<p class="error" id="email-error"></p><br><br>

				<input type="submit" value="Отправить">
				<input type="reset" value="Очистить">

			</form>
		</div>
		<div class="footer">Выполнил: <a href="mailto:kulaginds@yandex.ru">Дмитрий Кулагин</a>.</div>
	</div>
	
	<div id="modal">
		
		<div class="bg"></div>

		<div class="body">
			
			<p>Чтобы проверить, что вы не робот, введите символы, которые расположены в картинке, в поле ниже.</p>

			<p>Чтобы показать другие символы, кликните по картинке.</p>

			<form id="captcha-form">
				
				<img src="./kcaptcha/?<?php echo $session_name?>=<?php echo $session_id?>" border="0" id="captcha-img" title="Кликните, если нужно обновить картинку"><br>

				<input type="text" id="captcha-value"><br>
				<p class="error" id="captcha-error"></p><br>

				<input type="submit" value="Подтвердить">
				<input type="button" value="Отменить" id="modal_close">

			</form>

		</div>

	</div>

	<script src="./jquery-1.11.3.min.js"></script>
	<script>
	(function(w,$) {

		var validateEmail = function(email) {
		    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		    return re.test(email);
		}

		var handler = function(e) {
			console.log("handler");

			var fields = ["name", "message", "email"],
				i,
				name,
				field,
				value,
				error,
				has_error = false;

			for (i=0; i<fields.length; i++) {
				name = fields[i];
				field = $("#" + name);
				value = $.trim(field.val());
				error = $("#" + name + "-error");

				field.removeClass("error");
				error.empty();

				if (value.length == 0) {
					error.html("Заполните поле.");
					field.addClass("error");
					has_error = true;
				} else {
					if ((name == "email") && !validateEmail(value)) {
						error.html("Введите корректную эл. почту.");
						field.addClass("error");
						has_error = true;
					}
				}
			}

			if (!has_error)
				modalOpen();
			return false;
		};

		var modalOpen = function() {
			$("#modal").show();
		};

		var modalClose = function() {
			$("#modal").hide();
		};

		var updateCaptcha = function() {
			$("#captcha-img").attr("src", "./kcaptcha/?<?php echo $session_name?>=<?php echo $session_id?>&rand=" + Math.random());
		};

		var handlerCaptcha = function(e) {
			var el = $("#captcha-value"),
				val = $.trim(el.val()),
				err = $("#captcha-error")
				has_error = false;

			el.removeClass("error");
			err.html("");

			if (val.length == 0) {
				el.addClass("error");
				err.html("Введите символы с картинки выше.");
				has_error = true;
			}

			if (has_error)
				return false;

			$.post("<?php echo URL::site("ajax/captcha"); ?>", { value:val }, function(result) {
				if (result != 1) {
					el.addClass("error");
					err.html(result);
					has_error = true;
					updateCaptcha();
				} else {
					modalClose();
					submitter();
				}
			});

			return false;
		};

		var submitter = function() {
			var name = $("#name").val().replace(/(::)+/g, '&#58;&#58;'),
				message = $("#message").val().replace(/(::)+/g, '&#58;&#58;'),
				email = $("#email").val().replace(/(::)+/g, '&#58;&#58;'),
				captcha = $("#captcha-value").val().replace(/(::)+/g, '&#58;&#58;');

			var data = [name, message, email, captcha].join("::");
			
			$.post("<?php echo URL::site("ajax/send"); ?>", { data: data }, function(result) {
				alert(result);
			});
		};

		console.log("Ready!");
		$("#form").submit(handler);
		$("#captcha-form").submit(handlerCaptcha);
		$("#captcha-img").click(updateCaptcha);
		$("#modal_close").click(modalClose);

	})(window, jQuery);
	</script>
</body>
</html>
